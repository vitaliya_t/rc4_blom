# RC4_Blom

Реализация потокового шифра RC4 и обмена ключами по схеме Блома на языке Java для первой лабораторной работы по курсу "Криптографические протоколы" (осенний семестр 2018/2019 СПбГЭТУ).

студенты группы 4362

Георгица Андрей

Тищенко Виталия

* * *

RC4 cipher and Blom's scheme key exchange implementation in Java for the first lab work of сryptographic protocols course (fall semester of 2018/2019 academic year, Saint-Petersburg ETU)


Andrey Georgitsa

Tishchenko Vitaliya

* * *

Codeship: [ ![Codeship Status for vitaliya_t/rc4_blom](https://app.codeship.com/projects/40b23770-b10f-0136-a0ac-0297a0934853/status?branch=master)](https://app.codeship.com/projects/310544)

