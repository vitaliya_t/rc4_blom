

import com.eltech.cryptoproto.lab1.*;

import org.junit.Assert;
import org.junit.Test;
import java.math.BigInteger;

public class RC4_BLOM_Test {
        private BlomScheme blom;
        private RC4 rc4;

        @Test
        public void test1_byteArr(){
                blom=new BlomScheme(2,256);
                BigInteger[] A_openKey=blom.calcOpentId();
                BigInteger[] A_secKey=blom.calcSecretId(A_openKey);


                BigInteger[] B_openKey=blom.calcOpentId();
                BigInteger[] B_secKey=blom.calcSecretId(B_openKey);


                BigInteger mod=blom.getModule();

                BigInteger A_rc4_key=BlomScheme.calculateKey(A_secKey,B_openKey,mod);
                RC4 A_rc4=new RC4(A_rc4_key.toByteArray());
                String txt="Hello, RC4!";
                byte[] plainData=txt.getBytes();
                byte[] cipherData=A_rc4.encrypt(plainData);


                BigInteger B_rc4_key=BlomScheme.calculateKey(B_secKey,A_openKey,mod);
                RC4 B_rc4=new RC4(B_rc4_key.toByteArray());

                //checking byte arrays
                Assert.assertArrayEquals(B_rc4.decrypt(cipherData),plainData);
        }

        @Test
        public void test2_String(){
                blom=new BlomScheme(5,1024);
                BigInteger[] A_openKey=blom.calcOpentId();
                BigInteger[] A_secKey=blom.calcSecretId(A_openKey);

                BigInteger[] B_openKey=blom.calcOpentId();
                BigInteger[] B_secKey=blom.calcSecretId(B_openKey);

                BigInteger mod=blom.getModule();

                BigInteger A_rc4_key=BlomScheme.calculateKey(A_secKey,B_openKey,mod);
                RC4 A_rc4=new RC4(A_rc4_key.toByteArray());

                BigInteger B_rc4_key=BlomScheme.calculateKey(B_secKey,A_openKey,mod);
                RC4 B_rc4=new RC4(B_rc4_key.toByteArray());

                String txt="Hello, check the string!";
                byte[] cipherData=A_rc4.encrypt(txt.getBytes());

                //checking Strings
                Assert.assertEquals(new String(B_rc4.decrypt(cipherData)),txt );
        }

        @Test
        public void test3_RusString(){
                blom=new BlomScheme(3,1536);
                BigInteger[] A_openKey=blom.calcOpentId();
                BigInteger[] A_secKey=blom.calcSecretId(A_openKey);

                BigInteger[] B_openKey=blom.calcOpentId();
                BigInteger[] B_secKey=blom.calcSecretId(B_openKey);

                BigInteger mod=blom.getModule();

                BigInteger A_rc4_key=BlomScheme.calculateKey(A_secKey,B_openKey,mod);
                RC4 A_rc4=new RC4(A_rc4_key.toByteArray());

                BigInteger B_rc4_key=BlomScheme.calculateKey(B_secKey,A_openKey,mod);
                RC4 B_rc4=new RC4(B_rc4_key.toByteArray());

                String txt="Проверь и эту строку!";
                byte[] plainData=txt.getBytes();
                byte[] cipherData=A_rc4.encrypt(txt.getBytes());

                Assert.assertEquals(new String(B_rc4.decrypt(cipherData)),txt );
        }



        @Test (expected = IllegalArgumentException.class)
        public void test4_smallKey(){
                blom=new BlomScheme(2,16);
                BigInteger[] A_openKey=blom.calcOpentId();
                BigInteger[] B_openKey=blom.calcOpentId();

                BigInteger[] A_secKey=blom.calcSecretId(A_openKey);
                BigInteger[] B_secKey=blom.calcSecretId(B_openKey);

                BigInteger mod=blom.getModule();

                BigInteger A_rc4_key=BlomScheme.calculateKey(A_secKey,B_openKey,mod);
                RC4 A_rc4=new RC4(A_rc4_key.toByteArray()); //key is too short, exception should be thrown
        }


        @Test (expected = IllegalArgumentException.class)
        public void test5_bigKey(){
                blom=new BlomScheme(2,2080);
                BigInteger[] A_openKey=blom.calcOpentId();
                BigInteger[] B_openKey=blom.calcOpentId();

                BigInteger[] A_secKey=blom.calcSecretId(A_openKey);
                BigInteger[] B_secKey=blom.calcSecretId(B_openKey);

                BigInteger mod=blom.getModule();

                BigInteger A_rc4_key=BlomScheme.calculateKey(A_secKey,B_openKey,mod);
                RC4 A_rc4=new RC4(A_rc4_key.toByteArray()); //key is too long, exception should be thrown
        }




}
