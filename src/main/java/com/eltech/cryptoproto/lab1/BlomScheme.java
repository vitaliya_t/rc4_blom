package com.eltech.cryptoproto.lab1;

import java.math.BigInteger;
import java.util.Random;


public class BlomScheme {

    /**
     * Secret symmetric matrix
     */
    private final BigInteger[][] secrMatrix;

    /**
     * Module of the finite field
     */
    private final BigInteger module;

    /**
     * Bit length of the numbers to be randomized
     */
    private final int bigIntLength;

    /**
     * Exchanger's pseudorandom number generator
     */
    private final Random randomizer;


    /**
     * BigInteger randomizer
     * (BigInteger constructor wrapper)
     *
     * @return random BigInteger value of the necessary bit length
     *
     */

    private BigInteger randomBigInteger(){
        return new BigInteger(bigIntLength,randomizer);
    }

    /**
     *
     * Probable check if the BigInteger number is prime
     *              (BigInteger isProbablePrime method wrapper
     *              false probability is 8*10^-30)
     * @param number - BigInteger to be checked
     * @return true if given number is prime, false if it's composite
     *
     */
    private boolean isPrime(BigInteger number){
        return number.isProbablePrime(100);
    }

    /**
     * BigInteger randomizer
     * (BigInteger constructor wrapper)
     *
     * @return random BigInteger value of the necessary bit length
     *
     */



    /**
     * Blom's scheme key exchanger constructor
     *
     * @param size secret matrix size
     * @param length numbers bit length
     * @throws IllegalArgumentException if arguments are negative
     */
    public BlomScheme(int size, int length) throws IllegalArgumentException{
        if (size<1||length<1) throw new IllegalArgumentException("Negative arguments!");
        randomizer=new Random();
        secrMatrix=new BigInteger[size][size];
        bigIntLength=length;
        BigInteger number;
        while (true){
            number=randomBigInteger();
            if (isPrime(number)){
                module=number;
                break;
            }
        }
        randSecrMatr();
    }
    /**
     * Secret matrix randomization method
     *
     */
    private void randSecrMatr(){
        for (int i = 0; i <secrMatrix.length ; i++) {
            for (int j = i; j <secrMatrix.length; j++) {
                secrMatrix[i][j]=randomBigInteger().remainder(module);
                if (i!=j) secrMatrix[j][i]=secrMatrix[i][j];
            }
        }
    }

    /**
     * Send module to the user
     *
     * @return  Galois field module
     *
     */

    public BigInteger getModule(){
        return module;
    }




    /**
     * Randomize open key for the user
     *
     * @return BigInteger array containing the key and module
     *
     */

    public  BigInteger[] calcOpentId(){
        BigInteger[] result=new BigInteger[secrMatrix.length];
        for (int i = 0; i <result.length ; i++) {
            result[i]=randomBigInteger().remainder(module);
        }
        return result;
    }

    /**
     *
     * Calculate users's secret key
     * using secret matrix multiplication
     *
     * @param  openID array of BigInteger containing open key
     * @throws IllegalArgumentException if key length and matrix size are not corresponding
     * @return array of BigInteger containing secret key
     *
     */
    public  BigInteger[] calcSecretId(BigInteger[] openID){
        if (openID.length!=secrMatrix.length) throw new IllegalArgumentException("Wrong ID size!");
        BigInteger[] result=new BigInteger[secrMatrix.length];
        BigInteger buf;
        for (int i = 0; i <result.length ; i++) {
            result[i]=BigInteger.ZERO;
            for (int j = 0; j <result.length ; j++) {
                buf=secrMatrix[i][j].multiply(openID[j]);
                result[i]=result[i].add(buf).remainder(module);
                result[i]=result[i].remainder(module);
            }
        }
        return result;
    }


    /**
     * Shared key computation using open ID and secret key multiplication
     *
     *
     * @param a first key
     * @param b second key
     * @param module Galois field module
     * @throws IllegalArgumentException if a and b size are not the same
     * @return shared key in BigInteger format
     *
     */
    public static BigInteger calculateKey(BigInteger[] a, BigInteger[] b, BigInteger module)throws IllegalArgumentException {
        if (a.length!=b.length) throw new IllegalArgumentException("");
        BigInteger result=BigInteger.ZERO;
        BigInteger buf;
        for (int i = 0; i <a.length ; i++) {
            buf=a[i].multiply(b[i]).remainder(module);
            result=result.add(buf).remainder(module);
        }
        return result;
    }

}
