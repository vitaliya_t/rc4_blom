package com.eltech.cryptoproto.lab1;

public class RC4 {
    private final byte[] S = new byte[256];


    /**
     * RC4 encryptor constructor
     *
     * @param key byte array, containing the key
     * @throws IllegalArgumentException if the key size is not correct
     */

    public RC4(final byte[] key) throws IllegalArgumentException{
        if (key.length < 5 || key.length > 256) {
            throw new IllegalArgumentException("Wrong key size (5<=length<=256)");
        } else {
            for (int i = 0; i < 256; i++) {
                S[i] = (byte)(i&0xFF);
            }
            int j = 0;
            for (int i = 0; i < 256; i++) {
                j = (j + S[i] + key[i % key.length]) & 0xFF;
                S[i] ^= S[j];
                S[j] ^= S[i];
                S[i] ^= S[j];
            }
        }
    }

    /**
     * Changing the key to a new one
     *
     * @param new_key array of bytes, containing new key
     */
    public void resetKey(final byte[] new_key){
        if (new_key.length < 1 || new_key.length > 256) {
            throw new IllegalArgumentException("Wrong key size (1<=length<=256)");
        } else {
            for (int i = 0; i < 256; i++) {
                S[i] = (byte)(i&0xFF);
            }
            int j = 0;
            for (int i = 0; i < 256; i++) {
                j = (j + S[i] + new_key[i % new_key.length]) & 0xFF;
                S[i] ^= S[j];
                S[j] ^= S[i];
                S[i] ^= S[j];
            }
        }


    }


    /**
     * RC4 encryption algorithm
     *
     * @param plaintext byte array, containing the information to be encrypted
     * @return byte array, containing encrypted information
     *
     */
    public byte[] encrypt(final byte[] plaintext) {
        final byte[] ciphertext = new byte[plaintext.length];
        int i = 0, j = 0, k, t;
        byte[] _S=S;
        for (int counter = 0; counter < plaintext.length; counter++) {
            i = ((i + 1)%256)&0xFF;
            j = ((j + _S[i])%256)&0xFF;
            _S[i] ^= _S[j];
            _S[j] ^= _S[i];
            _S[i] ^= _S[j];
            t = ((_S[i] + _S[j])%256)&0xFF;
            k = _S[t];
            ciphertext[counter] = (byte)((plaintext[counter] ^ k)&0xFF);
        }
        return ciphertext;
    }

    /**
     * RC4 decryption (because RC4 is based on XOR
     *                 operation repeat encryption
     *                 algorithm with the same key)
     *
     * @param  ciphertext byte array, containing encrypted information
     * @return byte array, containing decrypted information
     */
    public byte[] decrypt(final byte[] ciphertext) {
        return encrypt(ciphertext);
    }

}
